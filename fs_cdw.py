#!/usr/bin/env python3

import elphmod
import numpy as np
import storylines

import model

cmap = elphmod.plot.colormap(
    (0.0, elphmod.plot.Color(0xff, 0xff, 0xff)),
    (1.0, elphmod.plot.Color(0x00, 0x00, 0xff)),
    )

elphmod.plot.Color.context = 'TeX'

kT = 0.025
nk = 120

a1, a2 = elphmod.bravais.translations()
b1, b2 = elphmod.bravais.reciprocals(a1, a2)

A1, A2 = 3 * a1, 3 * a2
B1, B2 = elphmod.bravais.reciprocals(A1, A2)

R = np.array([m * a1 + n * a2 for m in range(3) for n in range(3)])

d1 = 0.01996981
d2 = 0.01881739
d3 = 0.00924151

dR  = np.array([d1, d2, d1, d3, d2, d2, d3, d3, d1])
phi = np.array([+3, +1, -1, -5, +5, -3, -1, +3, -5]) * np.pi / 6

alpha = 1.0

R[:, 0] += alpha * dR * np.cos(phi)
R[:, 1] += alpha * dR * np.sin(phi)

h = model.hamiltonian_sym()
H = model.hamiltonian_cdw(R=R, a1=A1, a2=A2, a=1.0)

k_int = sorted(elphmod.bravais.irreducibles(nk))
k = 2 * np.pi * np.array(k_int, dtype=float) / nk
K = k * 3

e, u = elphmod.dispersion.dispersion(h, k, vectors=True)
E, U = elphmod.dispersion.dispersion(H, K, vectors=True)

e = e[..., :1]
u = u[..., :1]

E = E[..., :9]
U = U[..., :9]

W = elphmod.dispersion.unfolding_weights(
    k       = [k1 * b1 + k2 * b2 for k1, k2 in k],
    R       = R,
    U0      = u,
    U       = U,
    blocks0 = [slice(0, 3)] * len(R),
    blocks  = [slice(3 * i, 3 * i + 3) for i in range(len(R))],
    sgn     = +1,
    )

Delta = np.sum(elphmod.occupations.fermi_dirac.delta(E / kT) * W, axis=1)

Delta_full = np.full((nk, nk), np.nan)

for ik, (k1, k2) in enumerate(k_int):
    Delta_full[k1, k2] = Delta[ik]

elphmod.bravais.complete(Delta_full)

kxmax, kymax, kx, ky, Delta_BZ = elphmod.plot.toBZ(Delta_full,
    return_k=True, points=500, outside=0.0)

image = elphmod.plot.color(Delta_BZ, cmap)

e = elphmod.dispersion.dispersion_full(h, nk)

FS = []

for contour in elphmod.dos.isoline(e[:, :, 0])(0.0):
    FS.append(list(zip(*[k1 * b1 + k2 * b2 for k1, k2 in contour])))

if elphmod.MPI.comm.rank == 0:
    plot = storylines.Plot(
        width  = 0.0,
        height = 3.0,

        margin = 0.2,
        right  = 1.5,

        xyaxes = False,

        xmin = -kxmax,
        xmax = +kxmax,
        ymin = -kymax,
        ymax = +kymax,

        background = 'fs_cdw.png',

        lpos = 'RT',
        lopt = 'below left',
        )

    elphmod.plot.save(plot.background, image)

    for contour in FS:
        plot.line(*contour, color='black', dash_pattern='on 1mm off 0.5mm')

    plot.line(*zip(*elphmod.bravais.BZ()), thick=True)

    plot.line(label='CDW', color=cmap(1), line_width='1mm', line_cap='butt')
    plot.line(label='sym.', color='black', dash_pattern='on 1mm off 0.5mm')

    plot.save('fs_cdw', standalone=True, pdf=True)
