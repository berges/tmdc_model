#!/usr/bin/env python3

import elphmod
import numpy as np
import storylines

import model

H = model.hamiltonian_sym()

k, x, GMKG = elphmod.bravais.GMKG(1000, corner_indices=True)

e = elphmod.dispersion.dispersion(H, k)

if elphmod.MPI.comm.rank == 0:
    plot = storylines.Plot(
        width  = 4.0,
        height = 3.0,

        bottom = 0.5,
        left   = 1.0,
        margin = 0.2,

        ymin  = -1.0,
        ymax  =  4.0,
        ystep =  1.0,

        xticks = zip(x[GMKG], [r'$\Gamma$', 'M', 'K', r'$\Gamma$']),
        ylabel = r'$\varepsilon$ (eV)',
        )

    for point in x[GMKG[1:-1]]:
        plot.line(x=point, color='gray')

    plot.line(y=0.0, color='gray')

    plot.axes()

    for n in range(e.shape[1]):
        plot.line(x, e[:, n], very_thick=True, color='teal')

    plot.save('el_sym', standalone=True, pdf=True)
