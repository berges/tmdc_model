#!/usr/bin/env python3

import elphmod
import numpy as np
import storylines

import model

colormap = elphmod.plot.colormap(
    (0.0, elphmod.plot.Color(0.0, 1, 255, 'PSV')),
    (1.0, elphmod.plot.Color(5.5, 1, 255, 'PSV')),
    )

nk = 120

iq = 0
nu = 2
n = 0

q = np.array([(0.0, 2 * np.pi / 3)])

H = model.hamiltonian_sym()
D = model.dynmat_sym()
g = model.coupling_sym()

e, U = elphmod.dispersion.dispersion_full_nosym(H, nk, vectors=True)
w2, u = elphmod.dispersion.dispersion(D, q, vectors=True)
g_band = elphmod.elph.sample(g, q, nk, U, u)

g2_mesh = np.absolute(g_band[iq, nu, :, :, n, n]) ** 2

kxmax, kymax, kx, ky, g2_BZ = elphmod.plot.toBZ(g2_mesh,
    points=500, outside=np.nan, return_k=True)

image = elphmod.plot.color(g2_BZ, colormap, minimum=0.0)

if elphmod.MPI.comm.rank == 0:
    plot = storylines.Plot(
        title = '$g^2$',

        width  = 0.0,
        height = 3.0,

        left   = 0.5,
        top    = 0.5,
        margin = 0.2,

        xyaxes = False,

        xmin = -kxmax,
        xmax = +kxmax,
        ymin = -kymax,
        ymax = +kymax,

        background = 'elph_sym.png',
        )

    elphmod.plot.save(plot.background, image)

    a1, a2 = elphmod.bravais.translations()
    b1, b2 = elphmod.bravais.reciprocals(a1, a2)

    K = [(1, 1), (-1, 2), (-2, 1), (-1, -1), (1, -2), (2, -1), (1, 1)]
    K = np.array([k1 * b1 + k2 * b2 for k1, k2 in K]) / 3
    plot.line(*K.T, thick=True)

    Q = np.array([q1 * b1 + q2 * b2 for q1, q2 in q]) / (2 * np.pi)

    for (x, y), position, label in [
        (K[2],        'left',  r"$\mathrm K$"),
        (K[1] - Q[0], 'right', r"$\mathrm K' {-} \vec q$"),
        ]:
        plot.code(r'\fill (<x=%g>, <y=%g>) circle (0.5mm) node [%s] {%s};'
            % (x, y, position, label))

    plot.save('elph_sym', standalone=True, pdf=True)
