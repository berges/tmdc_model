#!/usr/bin/env python3

import elphmod
import numpy as np
import storylines

import model

D = model.dynmat_sym()

q, x, GMKG = elphmod.bravais.GMKG(1000, corner_indices=True)

w2 = elphmod.dispersion.dispersion(D, q)
w = elphmod.ph.sgnsqrt(w2)

if elphmod.MPI.comm.rank == 0:
    plot = storylines.Plot(
        width  = 4.0,
        height = 3.0,

        bottom = 0.5,
        left   = 1.0,
        margin = 0.2,

        ymin  =  0.0,
        ymax  = 30.0,
        ystep = 10.0,

        xticks = zip(x[GMKG], [r'$\Gamma$', 'M', 'K', r'$\Gamma$']),
        ylabel = r'$\omega$ (meV)',
        )

    for point in x[GMKG[1:-1]]:
        plot.line(x=point, color='gray')

    plot.axes()

    for n in range(w.shape[1]):
        plot.line(x, w[:, n] * 1e3, very_thick=True, color='purple')

    plot.save('ph_sym', standalone=True, pdf=True)
