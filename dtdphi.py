#!/usr/bin/env python3

from sympy import *

phi = sympify('phi')

R = sympify('''
    Matrix([
        [1,            0,             0],
        [0, cos(2 * phi), -sin(2 * phi)],
        [0, sin(2 * phi),  cos(2 * phi)],
        ])
    ''')

pprint(simplify(diff(R, phi) * R ** -1))
pprint(simplify(R * diff(R ** -1, phi)))
