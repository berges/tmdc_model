#!/usr/bin/env python3

import elphmod
import numpy as np
import storylines

import model

a1, a2 = elphmod.bravais.translations()
b1, b2 = elphmod.bravais.reciprocals(a1, a2)

A1, A2 = 3 * a1, 3 * a2
B1, B2 = elphmod.bravais.reciprocals(A1, A2)

R = np.array([m * a1 + n * a2 for m in range(3) for n in range(3)])

d1 = 0.01996981
d2 = 0.01881739
d3 = 0.00924151

dR  = np.array([d1, d2, d1, d3, d2, d2, d3, d3, d1])
phi = np.array([+3, +1, -1, -5, +5, -3, -1, +3, -5]) * np.pi / 6

alpha = 1.0

R[:, 0] += alpha * dR * np.cos(phi)
R[:, 1] += alpha * dR * np.sin(phi)

h = model.hamiltonian_sym()
H = model.hamiltonian_cdw(R=R, a1=A1, a2=A2, a=1.0)

k, x, GMKG = elphmod.bravais.GMKG(200, corner_indices=True)
K = 3 * k

e, u = elphmod.dispersion.dispersion(h, k, vectors=True, order=alpha)[:2]
E, U = elphmod.dispersion.dispersion(H, K, vectors=True, order=alpha)[:2]

W = elphmod.dispersion.unfolding_weights(
    k       = [k1 * b1 + k2 * b2 for k1, k2 in k],
    R       = R,
    U0      = u,
    U       = U,
    blocks0 = [slice(0, 3)] * len(R),
    blocks  = [slice(3 * i, 3 * i + 3) for i in range(len(R))],
    sgn     = +1,
    )

if elphmod.MPI.comm.rank == 0:
    plot = storylines.Plot(
        width  = 4.0,
        height = 3.0,

        bottom = 0.5,
        left   = 1.0,
        margin = 0.2,

        ymin  = -1.0,
        ymax  =  4.0,
        ystep =  1.0,

        xticks = zip(x[GMKG], [r'$\Gamma$', 'M', 'K', r'$\Gamma$']),
        ylabel = r'$\varepsilon$ (eV)',
        )

    for point in x[GMKG[1:-1]]:
        plot.line(x=point, color='gray')

    plot.line(y=0.0, color='gray')

    for n in range(E.shape[1]):
        plot.compline(x, E[:, n], W[:, n],
            colors     = ['teal'],
            cut        = True,
            draw       = 'none',
            protrusion = 1.0,
            thickness  = 0.07,
            shortcut   = 1.0,
            threshold  = 0.02,
            )

    plot.save('el_cdw', standalone=True, pdf=True)
