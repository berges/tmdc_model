#!/usr/bin/env python3

import elphmod
import numpy as np

eV = 1.602176634e-19
AA = 1e-10
u  = 1.66053906660e-27
h  = 6.62607015e-34

time_unit = np.sqrt(u / eV) * AA
hbar = h / (2 * np.pi * eV * time_unit)

def rotation(phi):
    return np.array([
        [np.cos(phi), -np.sin(phi), 0.0],
        [np.sin(phi),  np.cos(phi), 0.0],
        [        0.0,          0.0, 1.0],
        ])

def rotation_d(phi):
    return np.array([
        [1.0,             0.0,              0.0],
        [0.0, np.cos(2 * phi), -np.sin(2 * phi)],
        [0.0, np.sin(2 * phi),  np.cos(2 * phi)],
        ])

def onsite(e_z2=1.868, e_x2y2=2.387, **ignore):
    return np.diag([e_z2, e_x2y2, e_x2y2])

def hopping(
        r         =  3.387, # bond length (AA)
        a         =  3.387, # lattice parameter (AA)
        phi       =  0.000, # bond angle (rad)
        beta      = -5.000, # scaling exponent
        t_z2      = -0.064,
        t_z2_x2y2 =  0.308,
        t_z2_xy   =  0.471,
        t_x2y2    = -0.355,
        t_x2y2_xy = -0.276,
        t_xy      =  0.533,
        **ignore):

    t = np.array([
        [ t_z2,       t_z2_x2y2,  t_z2_xy  ],
        [ t_z2_x2y2,  t_x2y2,     t_x2y2_xy],
        [-t_z2_xy,   -t_x2y2_xy,  t_xy     ],
        ])

    if abs(3 * phi / np.pi % 2 - 1) < 0.5:
        t = t.T

    return (r / a) ** beta * rotation_d(phi).dot(t).dot(rotation_d(-phi))

A = np.array([
    [0, 0,  0],
    [0, 0, -2],
    [0, 2,  0],
    ])

def hopping_change(r=3.387, phi=0.0, beta=-5.0, **kwargs):
    t = hopping(r=r, phi=phi, beta=beta, **kwargs)

    dtdr = beta / r * t
    dtdphi = A.dot(t) - t.dot(A)

    dtdR = np.zeros((3, 3, 3))
    dtdR[0] = dtdr * np.cos(phi) - dtdphi / r * np.sin(phi)
    dtdR[1] = dtdr * np.sin(phi) + dtdphi / r * np.cos(phi)

    return dtdR

def spring(
    phi =  0.0, # bond angle (rad)
    Cx  = -0.758e-4,
    Cy  = -0.076e-4,
    Cz  = -0.127e-4,
    Cxy = -0.008e-4,
    **ignore):

    C = np.array([
        [ Cx,   Cxy,  0.0],
        [-Cxy,  Cy,   0.0],
        [ 0.0,  0.0,  Cz ],
        ])

    return rotation(phi).dot(C).dot(rotation(-phi))

def hamiltonian_sym( **kwargs):
    H0 = onsite()

    t1 = hopping(phi=0.0,           **kwargs)
    t2 = hopping(phi=2 * np.pi / 3, **kwargs)
    t3 = hopping(phi=4 * np.pi / 3, **kwargs)

    def H(k1=0.0, k2=0.0):
        e1 = np.exp(1j * k1)
        e2 = np.exp(1j * k2)

        t = t1 * e1 + t2 * e2 + t3 * (e1 * e2).conj()

        return H0 + t + t.T.conj()

    return H

def hamiltonian_cdw(R, a1, a2, cutoff=1.5, **kwargs):
    cells = [-1, 0, 1]

    t = np.zeros((len(cells), len(cells), 3 * len(R), 3 * len(R)))

    sub = [slice(3 * i, 3 * i + 3) for i in range(len(R))]

    for k in range(len(R)):
        for l in range(len(R)):
            for m in cells:
                for n in cells:
                    if k == l and m == n == 0:
                        t[m, n, sub[k], sub[l]] += onsite(**kwargs)
                    else:
                        x, y = m * a1 + n * a2 + R[k] - R[l]
                        r = np.sqrt(x * x + y * y)

                        if r < cutoff:
                            phi = np.arctan2(y, x)

                            t[m, n, sub[k], sub[l]] += hopping(r=r, phi=phi,
                                **kwargs)

    def H(k1=0.0, k2=0.0):
        H = np.empty(t.shape, dtype=complex)

        for m in cells:
            for n in cells:
                H[m, n] = t[m, n] * np.exp(1j * (m * k1 + n * k2))

        return(H.sum(axis=(0, 1)))

    return H

def dynmat_sym(**kwargs):
    C1 = spring(phi=0.0,           **kwargs)
    C2 = spring(phi=2 * np.pi / 3, **kwargs)
    C3 = spring(phi=4 * np.pi / 3, **kwargs)

    def D(q1=0.0, q2=0.0):
        e1 = np.exp(1j * q1)
        e2 = np.exp(1j * q2)

        C = C1 * (e1 - 1) + C2 * (e2 - 1) + C3 * ((e1 * e2).conj() - 1)

        return C + C.T.conj()

    return D

def coupling_sym(M=180.94788, **kwargs):
    dt1 = hopping_change(phi=0.0,           **kwargs)
    dt2 = hopping_change(phi=2 * np.pi / 3, **kwargs)
    dt3 = hopping_change(phi=4 * np.pi / 3, **kwargs)

    def g(k1=0.0, k2=0.0, q1=0.0, q2=0.0, **kwargs):
        e1 = np.exp(1j * k1)
        e2 = np.exp(1j * k2)
        E1 = np.exp(1j * q1)
        E2 = np.exp(1j * q2)

        dt = dt1 * e1 * (1 - E1) + dt2 * e2 * (1 - E2) \
            + dt3 * (e1 * e2 * (1 - E1 * E2)).conj()

        return hbar / np.sqrt(M) * (dt - dt.transpose(0, 2, 1).conj())

    return g
