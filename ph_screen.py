#!/usr/bin/env python3

import elphmod
import numpy as np
import storylines

import model

comm = elphmod.MPI.comm

nk = 36
nq = 12

kT = 0.025

scale = 0.67

H = model.hamiltonian_sym()
D = model.dynmat_sym()
g = model.coupling_sym()

q = sorted(elphmod.bravais.irreducibles(nq))
q = 2 * np.pi * np.array(q, dtype=float) / nq

e, U = elphmod.dispersion.dispersion_full_nosym(H, nk, vectors=True)

e = e[..., :1]
U = U[..., :1]

g_band = scale * elphmod.elph.sample(g, q, nk, U)

g2 = np.einsum('qxklnm,qyklnm->qxyklnm', g_band.conj(), g_band)

Pi = elphmod.diagrams.phonon_self_energy(q, e, g2, kT=kT)
Pi = np.reshape(Pi, (len(q), 3, 3))

D = elphmod.dispersion.sample(D, q)

ph = elphmod.ph.Model()
elphmod.ph.q2r(ph, D + Pi, q, nq, apply_asr=True)

q, x, GMKG = elphmod.bravais.GMKG(1000, corner_indices=True, mesh=True)

w2, u, order = elphmod.dispersion.dispersion(ph.D, q, vectors=True, order=True)
w = elphmod.ph.sgnsqrt(w2)

LTZ = elphmod.ph.polarization(u, q)

if comm.rank == 0:
    plot = storylines.Plot(
        width  = 4.0,
        height = 3.0,

        bottom = 0.5,
        left   = 1.0,
        margin = 0.2,

        ymin  = -10.0,
        ymax  =  30.0,
        ystep =  10.0,

        xticks = zip(x[GMKG], [r'$\Gamma$', 'M', 'K', r'$\Gamma$']),
        yformat = lambda y: '$%g\,\mathrm i$' % abs(y) if y < 0 else '$%g$' % y,
        ylabel = r'$\omega$ (meV)',

        lpos = 'lt',
        lopt = 'below right=-1mm',
        lbls = '8pt',
        )

    for point in x[GMKG[1:-1]]:
        plot.line(x=point, color='gray')

    plot.line(y=0, color='gray')

    colors = ['blue', 'cyan', 'orange']

    for nu in range(w.shape[1]):
        plot.compline(x, w[:, nu] * 1e3, LTZ[:, nu],
            colors     = colors,
            cut        = True,
            draw       = 'none',
            protrusion = 1.0,
            thickness  = 0.07,
            shortcut   = 1.0,
            threshold  = 0.02,
            )

    for label, color in zip('LTZ', colors):
        plot.line(label=label, color=color, line_cap='butt', line_width='0.7mm')

    plot.save('ph_screen', standalone=True, pdf=True)
